var requestHandlers = require('./requestHandlers');
var url             = require('url');

function routeRequest(request, response) {
  var pathname = url.parse(request.url).pathname;
  console.log('routing requested path: ' + pathname);

  if (pathname === '/') {
    requestHandlers.main(response);
  } else if (pathname === '/dev') {
    requestHandlers.dev(request, response);
  } else if (pathname === '/dev/save') {
    requestHandlers.devSave(request, response);
  } else if (pathname === '/dev/search') {
    requestHandlers.devSearch(request, response);
  } else if (pathname === '/command') {
    requestHandlers.handleCommand(request, response);
  } else {
    requestHandlers.error(response);
  }
}

exports.routeRequest = routeRequest;

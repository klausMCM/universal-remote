var child = require('child_process');

/**
 * sendCommand
 *
 * @param count 
 * @param device - Device to be controlled.
 * @param command - Command to send to device.
 * @param timeout - The time (in seconds) the program should wait before moving onto 
 * something else, like the next command. Needed because not all commands may
 * be captured by a device if they are sent without some delay between sending.
 * ex.  sending commands to tv
 *      The tv takes a few seconds to turn on after pressing the power button.
 *      If you send another command right after turning on the power, such as
 *      the one selecting the tv's input, the input command would be sent while
 *      the tv is still turning on. Therefore, there should be a delay before
 *      sending the input command.
 * @return {undefined}
 */
function sendCommand(count, device, command, timeout) {
  var parameters = '--count 1 ' + 'SEND_ONCE ' + device + ' ' +  command;
  var i          = 0;

  while (i < count) {
    child.exec('irsend ' + parameters, function (error, stdout, stderr) {
      console.log('stdout: ' + stdout);
      console.log('stderr: ' + stderr);
      if (error !== null) {
        console.log('exec error: ' + error);
      }
    });
    setTimeout(function () {}, timeout);
    i++;
  }
}

exports.sendCommand = sendCommand;

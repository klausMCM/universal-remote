var url         = require('url');
var querystring = require('querystring');
var fs          = require('fs');
var cheerio     = require('cheerio');
var commander   = require('./commander');
var output      = '../files/out.js';
var mainPage    = './html/index.html';
var devPage     = './html/dev/index.html';

////////////////////////////////////////////////////////////////////////////////
function main(response) {
  fs.readFile(mainPage, function (err, data) {
    response.writeHead(200, {'content-type':'text/html'});
    response.write(data);
    response.end();
  });
}

function dev(request, response) {
  console.log('query is: ' + request.url);
  fs.readFile(devPage, function (err, data) {
    response.writeHead(200, {'content-type':'text/html'});
    response.write(data);
    response.end();
  });
}

function error(response) {
  response.writeHead(400, {'content-type':'text/plain'});
  response.write('processing error');
  response.end();
}
////////////////////////////////////////////////////////////////////////////////
function writeToFile(components) {
  fs.readFile(output, encoding='utf8', function (err, data) {
    if (err) {
      throw err;
    } else {
      var fileContents = JSON.parse(data);
      fileContents.push(components);
      fs.writeFile(output, JSON.stringify(fileContents), function (err) {
        if (err) {
          throw err;
        }
      });
    }
  });
}

function devSave(request, response) {
  var query = url.parse(request.url).query;
  if (query !== null) {
    var components = querystring.parse(query);
    console.log('request handler sees this');
    console.log('irsend          : ' + components.irsend);
    console.log('easy            : ' + components.easy);
    console.log('device          : ' + components.device);
    components.irsend = components.irsend.toUpperCase();
    writeToFile(components);
  }
  dev(request, response);
}
////////////////////////////////////////////////////////////////////////////////
function createSearchResult(selectedDevice, callback) {
  var result = '';
  fs.readFile(output, function (err, data) {
    if (err) {
      throw err;
    }
    var dbFile = JSON.parse(data);
    for (var i = 0, item; item=dbFile[i]; i++) {
      if (item.device === selectedDevice) {
        console.log('match found');
        result += '<br>' + item.irsend + ': ' + item.easy;
      }
    }
    console.log('check point - createSearchResult: ' + result);
    callback(result);
  });
}

function devSearch(request, response) {
  // TODO: not done yet
  var query          = url.parse(request.url).query;
  var selectedDevice = querystring.parse(query).device;

  console.log('device selected: ' + selectedDevice);
  fs.readFile(devPage, function (err, data) {
    if (err) {
      throw err;
    }
    createSearchResult(selectedDevice, function (result) {
      var $ = cheerio.load(data);
      console.log('the result: ' + result);
      $('div').text('commands for ' + selectedDevice + result);
      response.writeHead(200, {'content-type':'text/html'});
      response.write($.html());
      response.end();
    });
  });
}
////////////////////////////////////////////////////////////////////////////////
function handleCommand(request, response) {
  var query          = url.parse(request.url).query;
  var tv             = '42LN5700';
  var receiver       = 'RX-V363';
  var submittedValue = querystring.parse(query).command;
  console.log(request.url);
  console.log(query);

  if (submittedValue === 'tvOn') {
    commander.sendCommand(1, tv, 'KEY_POWER', 5000);
  } else if (submittedValue === 'receiverOn') {
    commander.sendCommand(1, receiver, 'KEY_POWER', 5000);
  } else if (submittedValue === 'receiverOff') {
    commander.sendCommand(1, receiver, 'KEY_POWER2', 5000);
  } else if (submittedValue === 'audioComputer') {
    commander.sendCommand(1, receiver, 'KEY_DVD', 5000);
  } else if (submittedValue === 'volumeUp') {
    commander.sendCommand(5, receiver, 'KEY_VOLUMEUP', 5000);
  } else if (submittedValue === 'volumeDown') {
    commander.sendCommand(5, receiver, 'KEY_VOLUMEDOWN', 5000);
  }
  main(response);
}
////////////////////////////////////////////////////////////////////////////////
exports.main          = main;
exports.dev           = dev;
exports.error         = error;
exports.devSave       = devSave;
exports.devSearch     = devSearch;
exports.handleCommand = handleCommand;

var http          = require('http');
var router        = require('./router');

function onRequest(request, response) {
  console.log('request received for: ' + request.url);
  router.routeRequest(request, response);
}

function start() {
  http.createServer(onRequest).listen(8888);
}

exports.start = start;

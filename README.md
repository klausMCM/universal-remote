# HOME OF THE UNIVERSAL REMOTE (IR/RF) PROGRAM

This is a web interface to control a IR remote control module capable
of learning new commands from consumer remote controls. The module is attached
to a raspberry pi and has a IR LED and IR sensor.

The raspberry pi will also control a radio module used to control remote
controlled power outlets.

The IR and radio modules are self-made.
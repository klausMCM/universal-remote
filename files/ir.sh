#!/bin/bash - 
#===============================================================================
#
#          FILE: ir.sh
# 
#         USAGE: ./ir.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (), 
#  ORGANIZATION: 
#       CREATED: 14/10/14 22:16
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

irsend SEND_ONCE yam KEY_POWER
sleep 1

irsend SEND_ONCE lg KEY_POWER
sleep 10

irsend --count=20 SEND_ONCE yam KEY_VOLUMEDOWN
sleep 1
irsend --count=20 SEND_ONCE yam KEY_VOLUMEDOWN
sleep 1
irsend --count=20 SEND_ONCE yam KEY_VOLUMEDOWN
sleep 1
irsend --count=20 SEND_ONCE yam KEY_VOLUMEDOWN
sleep 1
irsend --count=20 SEND_ONCE yam KEY_VOLUMEDOWN
sleep 3

irsend --count=20 SEND_ONCE yam KEY_VOLUMEUP
sleep 1
irsend --count=20 SEND_ONCE yam KEY_VOLUMEUP
sleep 1
irsend --count=20 SEND_ONCE yam KEY_VOLUMEUP
sleep 1
irsend --count=3 SEND_ONCE yam KEY_VOLUMEUP
sleep 1

irsend SEND_ONCE lg KEY_CONNECT
sleep 1

irsend SEND_ONCE lg KEY_LEFT
sleep 1
irsend SEND_ONCE lg KEY_LEFT
sleep 1
irsend SEND_ONCE lg KEY_LEFT
sleep 1
irsend SEND_ONCE lg KEY_LEFT
sleep 1
irsend SEND_ONCE lg KEY_LEFT
sleep 1

irsend SEND_ONCE lg KEY_RIGHT
sleep 1
irsend SEND_ONCE lg KEY_RIGHT
sleep 1

irsend SEND_ONCE lg KEY_OK
sleep 1
irsend SEND_ONCE yam KEY_DVD
